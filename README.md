Thank you for taking a look at Timewarrior!

Timewarrior is a time tracking utility that offers simple stopwatch features as
well as sophisticated calendar-base backfill, along with flexible reporting. It
is a portable, well supported and very active Open Source project.

Although Timewarrior is a new project there is extensive online documentation.
You'll find all the details at:

    http://taskwarrior.org/docs/timewarrior

At the site you'll find online documentation, downloads, news and more.

Your contributions are especially welcome. Whether it comes in the form of code
patches, ideas, discussion, bug reports, encouragement or criticism, your input
is needed.

For support options, take a look at:

    http://taskwarrior.org/support

Please send your code patches to:

    support@taskwarrior.org

Consider joining bug.tasktools.org and participating in the future of Timewarrior.

---

Timewarrior is released under the MIT license. For details check the LICENSE
file.
